# Tetris Tadishi
*Our version of classic tetris game*
  
<img src="./images/javascript.svg" height="70">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="./images/html-5.svg" height="70">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="./images/css-3.svg" height="70">&nbsp;&nbsp;&nbsp;
<img src="./images/bootstrap .svg" height="100">&nbsp;&nbsp;&nbsp;
<img src="./images/nodejs.svg" height="70">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="./images/express.svg" height="50">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="./images/postgresql-icon.svg" height="70">

## Try it out

```
$ git clone https://gitlab.com/tanvee_balhara/tetris.git 
$ cd tetris/server  
$ npm install  
$ nodemon index.js  
```
Go to [http://localhost:3000/tetris](http://localhost:3000/tetris)